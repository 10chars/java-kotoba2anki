

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.BorderFactory;
import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JScrollPane;

public class HelpActionListener implements ActionListener{

	@Override
	public void actionPerformed(ActionEvent e) {
		
    	JFrame helpFrame = new JFrame();
    	helpFrame.setSize(800, 600);
    	
    	JEditorPane helpEditorPane = new JEditorPane();
    	helpEditorPane.setContentType("text/html");
    	helpEditorPane.setSize(800,600);
    	helpEditorPane.setBorder(BorderFactory.createEmptyBorder(15, 15, 15, 15));
    	
    	java.net.URL helpURL = getClass().getResource("helpHTML.html");
    	                                
    	if (helpURL != null) {
    		try {
    	        helpEditorPane.setPage(helpURL);

    	    } catch (IOException e2) {
    	        System.err.println("Attempted to read a bad URL: " + helpURL);
    	    }
    	} else {
    	    System.err.println("Couldn't find file");
    	   
    	}

    	//	Put the editor pane in a scroll pane.
    	
    	JScrollPane helpScrollPane = new JScrollPane(helpEditorPane);
    	helpScrollPane.setVerticalScrollBarPolicy(
    	                JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
    	helpScrollPane.setPreferredSize(new Dimension(800, 600));
    	helpScrollPane.setMinimumSize(new Dimension(10, 10));
    	
    	Dimension dim = MainGUI.getDim();
        int x = (dim.width-800)/2;
        int y = (dim.height-600)/2;
    	helpFrame.add(helpScrollPane);
    	helpFrame.setLocation(x, y);
    	helpFrame.setVisible(true);
		
	}

}
