


import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import java.util.Scanner;
import java.util.StringTokenizer;

import javax.swing.*;



public class MainGUI implements ActionListener  {

	private static Dimension dim;
	private JLabel iconJLabel;
	private JPanel leftPanel;
	private JPanel middlePanel;
	private JPanel buttonPanel;
	private JPanel topPanel;
	private JMenuItem aboutMenuItem;
	private JMenuItem helpMenuItem;
	private JMenu menuBarHelp;
	private JLabel exampleLabel;
	private JRadioButton jRadioButton4;
	private JRadioButton jRadioButton3;
	private JRadioButton jRadioButton2;
	private JRadioButton jRadioButton1;
	private JTextArea statusTextField;
	private JPanel statusPanel;
	private JPanel spacerPanel;
	private JPanel radioButtonPanel;
	private JPanel rightPanel;
	private JButton openButton;
	private JButton exitButton;
	private JMenuItem exitFileMenuItem;
	private JMenuItem openFileMenuItem;
	private JMenu menuBarFile;
	private JMenuBar menuBar;
	private JFrame mainWindow;
	


	
	public MainGUI() {
		
		initGUI();
		
	}
	
	private void initGUI() {
		try {
			{	
				mainWindow = new JFrame();
				mainWindow.setResizable(false);
				mainWindow.setTitle("Kotoba2Anki");
				mainWindow.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			
								
				topPanel = new JPanel();
				FlowLayout topPanelLayout = new FlowLayout();
				mainWindow.getContentPane().add(topPanel, BorderLayout.NORTH);
				topPanel.setLayout(topPanelLayout);
	
				{
					iconJLabel = new JLabel();
					topPanel.add(iconJLabel);
					iconJLabel.setIcon(new ImageIcon(getClass().getClassLoader().getResource("icon.png")));
				}
			}
			{
				buttonPanel = new JPanel();
				FlowLayout jPanel2Layout = new FlowLayout();
				mainWindow.getContentPane().add(buttonPanel, BorderLayout.SOUTH);
				buttonPanel.setLayout(jPanel2Layout);
				{
					exitButton = new JButton();
					buttonPanel.add(exitButton);
					exitButton.setText("Exit");
			        exitButton.addActionListener(new ActionListener() { 
			            public void actionPerformed(ActionEvent e)
			            {
			            	System.exit(0);
			            }
			        });
				}
				{
					openButton = new JButton();
					buttonPanel.add(openButton);
					openButton.setText("Open");
			        openButton.addActionListener(new ActionListener() { 
			            public void actionPerformed(ActionEvent e)
			            {
			            	fileSelect();
			            }
			        });
				}
			}
			{
				middlePanel = new JPanel();
				BoxLayout jPanel3Layout = new BoxLayout(middlePanel, javax.swing.BoxLayout.X_AXIS);
				middlePanel.setLayout(jPanel3Layout);
				mainWindow.getContentPane().add(middlePanel, BorderLayout.CENTER);
				{
					leftPanel = new JPanel();
					BoxLayout leftPanelLayout = new BoxLayout(leftPanel, javax.swing.BoxLayout.Y_AXIS);
					leftPanel.setLayout(leftPanelLayout);
					middlePanel.add(leftPanel);
					leftPanel.setPreferredSize(new java.awt.Dimension(175, 252));
					leftPanel.setBorder(BorderFactory.createTitledBorder("Options"));
					{
						radioButtonPanel = new JPanel();
						BoxLayout jPanel6Layout = new BoxLayout(radioButtonPanel, javax.swing.BoxLayout.Y_AXIS);
						radioButtonPanel.setLayout(jPanel6Layout);
						leftPanel.add(radioButtonPanel);

							jRadioButton1 = new JRadioButton();
							radioButtonPanel.add(jRadioButton1);
							jRadioButton1.setText("Hiragana, Kanji / Definition");
							jRadioButton1.setActionCommand("option1");
							jRadioButton1.addActionListener(this);
							
							jRadioButton1.setSelected(true);
						
							jRadioButton2 = new JRadioButton();
							radioButtonPanel.add(jRadioButton2);
							jRadioButton2.setText("Hiragana / Definition");
							jRadioButton2.setActionCommand("option2");
							jRadioButton2.addActionListener(this);
							
						
							jRadioButton3 = new JRadioButton();
							radioButtonPanel.add(jRadioButton3);
							jRadioButton3.setText("Kanji / Definition");
							jRadioButton3.setActionCommand("option3");
							jRadioButton3.addActionListener(this);
						
						
							jRadioButton4 = new JRadioButton();
							radioButtonPanel.add(jRadioButton4);
							jRadioButton4.setText("Kanji / Hiragana");
							jRadioButton4.setActionCommand("option4");
							jRadioButton4.addActionListener(this);
						
							ButtonGroup buttonGroup = new ButtonGroup();
							
							buttonGroup.add(jRadioButton1);
							buttonGroup.add(jRadioButton2);
							buttonGroup.add(jRadioButton3);
							buttonGroup.add(jRadioButton4);
							
					}
					{
						spacerPanel = new JPanel();
						spacerPanel.setPreferredSize(new Dimension());
						leftPanel.add(spacerPanel);
				
					}
					{
						statusPanel = new JPanel();
						FlowLayout jPanel8Layout = new FlowLayout();
						statusPanel.setLayout(jPanel8Layout);
						statusPanel.setEnabled(false);
						leftPanel.add(statusPanel);
						
						
						statusTextField = new JTextArea();
						statusTextField.setEditable(false);
						
						JScrollPane statusScroll = new JScrollPane(statusTextField);
				        statusScroll.getVerticalScrollBar().addAdjustmentListener(new AdjustmentListener(){
				        	public void adjustmentValueChanged(AdjustmentEvent e){
				        	statusTextField.select(statusTextField.getHeight()+1000,0);
				        	}});
						
						
						statusScroll.setBorder(BorderFactory.createEtchedBorder());
						statusScroll.setPreferredSize(new java.awt.Dimension(259, 75));
						
						statusPanel.add(statusScroll);
						
						
						
					}
				}
				
					rightPanel = new JPanel();
					FlowLayout jPanel4Layout = new FlowLayout();
					rightPanel.setLayout(jPanel4Layout);
					
					middlePanel.add(rightPanel);
					rightPanel.setBorder(BorderFactory.createTitledBorder("Example"));
					rightPanel.setPreferredSize(new java.awt.Dimension(185, 252));
					
						exampleLabel = new JLabel(createImageIcon("option1.png"));
						exampleLabel.setBorder(BorderFactory.createLineBorder(Color.BLACK));
						exampleLabel.setBorder(BorderFactory.createEtchedBorder());
						rightPanel.add(exampleLabel);
						exampleLabel.setPreferredSize(new java.awt.Dimension(241, 177));
						exampleLabel.setVisible(true);
					
				
			}
			mainWindow.setSize(619, 418);
			{
				menuBar = new JMenuBar();
				mainWindow.setJMenuBar(menuBar);
				{
					menuBarFile = new JMenu();
					menuBar.add(menuBarFile);
					menuBarFile.setText("File");
					{
						openFileMenuItem = new JMenuItem();
						menuBarFile.add(openFileMenuItem);
						openFileMenuItem.setText("Open");
						openFileMenuItem.addActionListener(new ActionListener() { 
				            public void actionPerformed(ActionEvent e)
				            {
				            	fileSelect();
				            }
				        });
					}
					{
						exitFileMenuItem = new JMenuItem();
						menuBarFile.add(exitFileMenuItem);
						exitFileMenuItem.setText("Exit");
						exitFileMenuItem.addActionListener(new ActionListener() { 
				            public void actionPerformed(ActionEvent e)
				            {
				            	System.exit(0);
				            }
				        });
					}

				}
				{
					menuBarHelp = new JMenu();
					menuBar.add(menuBarHelp);
					menuBarHelp.setText("About");
					{
						helpMenuItem = new JMenuItem();
						menuBarHelp.add(helpMenuItem);
						helpMenuItem.setText("Help");
						helpMenuItem.addActionListener(new HelpActionListener());
					}
					{
						aboutMenuItem = new JMenuItem();
						menuBarHelp.add(aboutMenuItem);
						aboutMenuItem.setText("About");
						aboutMenuItem.addActionListener(new AboutActionListener());
					}

				}

			}
			
			dim = Toolkit.getDefaultToolkit().getScreenSize();
		    int x = (dim.width-619)/2;
	        int y = (dim.height-418)/2;
	    	
	    	mainWindow.setLocation(x, y);
			mainWindow.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
    private void fileSelect(){
     	
    	JFrame fileChooserFrame = new JFrame();
    	JFileChooser chooser = new JFileChooser();
    	chooser.setFileFilter(new TextFileFilter());
    	fileChooserFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	    fileChooserFrame.pack();

    	int result = chooser.showOpenDialog(fileChooserFrame);

    	switch (result) {
    	  case JFileChooser.APPROVE_OPTION:
    	  
    		  File file = chooser.getSelectedFile();
    		  statusTextField.append("Opening file to parse...\n");
    		  Parser(file);    		  
    	    break;
    	  case JFileChooser.CANCEL_OPTION:
    		  statusTextField.append("User cancelled...\n");
    	    
    	    break;
    	  case JFileChooser.ERROR_OPTION:
    	   
    	    break;
    	}
	
    }

    
    private void Parser(File inputFile){

		try {
			
			
			//	Create new file name.
			String oldfilePath = inputFile.getAbsolutePath();
			String newExtension = "";
			if(jRadioButton1.isSelected()){newExtension = " (Format1).txt";}
			if(jRadioButton2.isSelected()){newExtension = " (Format2).txt";}
			if(jRadioButton3.isSelected()){newExtension = " (Format3).txt";}
			if(jRadioButton4.isSelected()){newExtension = " (Format4).txt";}
			
			String newFilePath = oldfilePath.replaceAll(".txt", newExtension);
			
			//	Set up scanner
			Scanner checkScanner = new Scanner(inputFile,"UTF-8");
			checkScanner.useDelimiter("/t");
			
			//Check file format
			
			while(checkScanner.hasNextLine()){
				String checkThis = checkScanner.nextLine();
				StringTokenizer token = new StringTokenizer(checkThis,"\t");
				int checkCounter = token.countTokens();
				System.out.println(checkCounter);
				if(checkCounter < 5 || checkCounter > 6){throw new Exception();}
			}
			
			Scanner sc = new Scanner(inputFile,"UTF-8");
			sc.useDelimiter("/t");
			
			//	Set up new file
			Charset utf8_cs = Charset.forName("UTF-8");
			
			OutputStreamWriter osw = new OutputStreamWriter(new FileOutputStream(newFilePath), utf8_cs);

			//Check for entries with no kanji (5 field instead of 6)
			while(sc.hasNextLine()){ 
					
				String lineRaw = sc.nextLine();
				StringTokenizer token = new StringTokenizer(lineRaw,"\t");
				
				int count = token.countTokens();
				
				//Option 1 (6 tokens)
				
				if(count == 6 && jRadioButton1.isSelected()){
				
				token.nextToken();
				token.nextToken();
				String a3 = token.nextToken();
				String a4 = token.nextToken();
				String a5 = token.nextToken();
				token.nextToken();
				
				osw.write(a4+"  "+"("+ a3+")"+"\t"+a5+"\n");
				
				//Option 2 (6 tokens)
				
				}else if(count == 6 && jRadioButton2.isSelected()){
					
					token.nextToken();
					token.nextToken();
					token.nextToken();
					String a4 = token.nextToken();
					String a5 = token.nextToken();
					token.nextToken();
					
					osw.write(a4+"  "+"\t"+a5+"\n");
				
				//Option 3	(6 tokens)
				
				}else if(count == 6 && jRadioButton3.isSelected()){
					
					token.nextToken();
					token.nextToken();
					String a3 = token.nextToken();
					token.nextToken();
					String a5 = token.nextToken();
					token.nextToken();
					
					osw.write(a3+"\t"+a5+"\n");
				
				//Option 4 (6 tokens)
					
				}else if(count == 6 && jRadioButton4.isSelected()){
					
					token.nextToken();
					token.nextToken();
					String a3 = token.nextToken();
					String a4 = token.nextToken();
					token.nextToken();
					token.nextToken();
					
					osw.write(a3+"\t"+a4+"\n");
				
				//Option 1 (5 tokens)	
					
				}else if(count == 5 && jRadioButton1.isSelected()){
					token.nextToken();
					token.nextToken();
					String a3 = token.nextToken();
					String a4 = token.nextToken();
					token.nextToken();
				
					osw.write(a3+"  "+"*"+"\t"+a4+"\n");
				
				//Option 2 (5 tokens)
					
				}else if(count == 5 && jRadioButton2.isSelected()){
					token.nextToken();
					token.nextToken();
					String a3 = token.nextToken();
					String a4 = token.nextToken();
					token.nextToken();
				
					osw.write(a3+"\t"+a4+"\n");
				
					//Option 3 (5 tokens)	
					
				}else if(count == 5 && jRadioButton3.isSelected()){
					token.nextToken();
					token.nextToken();
					String a3 = token.nextToken();
					String a4 = token.nextToken();
					token.nextToken();
				
					osw.write(a3+" (Kana Only)"+"\t"+a4+"\n");
				
					//Option 4 (5 tokens)	
					
				}else if(count == 5 && jRadioButton4.isSelected()){
					token.nextToken();
					token.nextToken();
					String a3 = token.nextToken();
					token.nextToken();
					token.nextToken();
				
					osw.write(a3+" (Kana Only)"+"\t"+a3+"\n");
				}
				}
						
			osw.close();
			statusTextField.append("File successfully created!\n");
			
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			statusTextField.append("[Error] - Check File Format!\n");
		}
	}
    
    public static Dimension getDim(){
    	return dim;
    	
    }
    

	@Override
	public void actionPerformed(ActionEvent e) {
		
		exampleLabel.setIcon(createImageIcon(e.getActionCommand()+".png"));
		
		
	}
	
    protected static ImageIcon createImageIcon(String path) {
        java.net.URL imgURL = MainGUI.class.getResource(path);
        if (imgURL != null) {
            return new ImageIcon(imgURL);
        } else {
            System.err.println("Couldn't find file: " + path);
            return null;
        }
    }

}
