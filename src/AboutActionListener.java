

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.JEditorPane;
import javax.swing.JFrame;

public class AboutActionListener implements ActionListener {

	@Override
	public void actionPerformed(ActionEvent e) {
	
		JFrame aboutFrame = new JFrame();
    	aboutFrame.setSize(300, 220);
    	aboutFrame.setResizable(false);
    	
    	JEditorPane aboutEditorPane = new JEditorPane();
     	aboutEditorPane.setContentType("text/html");
    	aboutEditorPane.setEditable(false);
    	aboutEditorPane.setSize(300, 220);	
    	java.net.URL helpURL = getClass().getResource("aboutHTML.html");
    	                                
    	if (helpURL != null) {
    		try {
    		
    	        aboutEditorPane.setPage(helpURL);
    	    } catch (IOException e2) {
    	        System.err.println("Attempted to read a bad URL: " + helpURL);
    	      
    	    }
    	} else {
    	    System.err.println("Couldn't find file");
    	    
    	}
    	
    	Dimension dim = MainGUI.getDim();
        int x = (dim.width-200)/2;
        int y = (dim.height-120)/2;
    	aboutFrame.add(aboutEditorPane);
    	aboutFrame.setLocation(x, y);
    	aboutFrame.setVisible(true);
    
		
	}

}
